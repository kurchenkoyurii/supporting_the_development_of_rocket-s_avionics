#include "secondwindow.h"
#include "ui_secondwindow.h"
#include <QMessageBox>
SecondWindow::SecondWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SecondWindow)
{
    ui->setupUi(this);
}

SecondWindow::~SecondWindow()
{
    delete ui;
}

void SecondWindow::on_pushButton_clicked()
{
  QString password = ui->password->text();
  if(password=="ska"){
      QMessageBox::information(this,"Success!","Zapałka odpalona");
  }else
      QMessageBox::warning(this,"Warning!","Żle wprowadzone hasło. Odpalenie zapałki przerwano!");

}

